'use strict';

/*Теоретичні питання
1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval().
setTimeout() - спрацьовує лише один раз.
setInterval() - спрацьовує стільки разів, скільки зможе, у заданий інтервал часу. 


2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
Якщо в функцію setTimeout() передати нульову затримку, вона спрацює так швидко, наскільки це можливо, але після виконання основного коду. 

3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
Тому що функція залишається в пам'яті до тих пір, поки ми не викликаєм clearInterval(). Щоб не засоряти пам'ять рекомендується відхиляти виклик setInterval 
за допомогою clearInterval(), якщо виклик більше не потрібен.
*/

const start = document.createElement('button');  // створюю кнопку start
start.innerHTML = 'Подивитися картинки';
start.className = 'start';
document.body.prepend(start);

const stop = document.createElement('button');    // створюю кнопку stop
stop.innerHTML = 'Припинити показ';
stop.className = 'stop';
start.after(stop);

const div = document.createElement('div');      // створюю div - батьківський єлемент для стилізації кнопок
div.className = 'button_wrapper';
document.body.prepend(div);
div.append(start);
div.append(stop);

let wrap = document.querySelector('.images-wrapper');      // створюю block щоб до запуску програми не було фоток
let none = document.createElement('img');
none.classList = "block";
wrap.prepend(none);

function showImg(){                                                   // функція для зміни картинок
const images = document.querySelectorAll('.image-to-show');
let first = images[0];
let last = images[images.length-1];
let block = document.querySelector('.block');

if(block !== last){
    block.classList.remove('block');
    block.nextElementSibling.classList.add('block');
}
else{
    last.classList.remove('block');
    first.classList.add('block');
}
}


let timer;

start.addEventListener('click', function(){
    showImg();
timer = setInterval(showImg, 3000);
start.disabled = true;
stop.disabled = false;
});

stop.addEventListener('click', function() {
    clearInterval(timer);
    start.disabled = false;
    stop.disabled = true;
});








